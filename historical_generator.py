import json
from datetime import datetime, timedelta

import requests

from dealAlgorithm import RandomDealData
from generator_app import daoUrl


def generate_historical_data(timelimit):
    data_generator = RandomDealData()
    instrument_list = data_generator.createInstrumentList()
    current_record_date = datetime.now() - timedelta(days=365)
    while current_record_date < timelimit:
        random_deal_str = data_generator.createRandomData(instrument_list)
        random_deal_dict = json.loads(random_deal_str)
        random_deal_dict["time"] = current_record_date.strftime("%d-%b-%Y (%H:%M:%S.%f)")
        random_deal_str = json.dumps(random_deal_dict)
        print(random_deal_str)
        requests.post(url=daoUrl, json=random_deal_str)
        current_record_date += timedelta(hours=1)


if __name__ == '__main__':
    time = datetime.now()
    generate_historical_data(time)
