import logging
import requests
from dealAlgorithm import RandomDealData

logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

daoUrl = "http://localhost:8081/dealdata"
webserviceUrl = "http://localhost:8080/dealdata"
if __name__ == '__main__':
    data_generator = RandomDealData()
    instrument_list = data_generator.createInstrumentList()
    while True:
        random_deal = data_generator.createRandomData(instrument_list)
        print(random_deal)
        requests.post(url=daoUrl, json=random_deal)
        requests.post(url=webserviceUrl, json=random_deal)
